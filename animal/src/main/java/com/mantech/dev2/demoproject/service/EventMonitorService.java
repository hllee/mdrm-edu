package com.mantech.dev2.demoproject.service;

import com.mantech.dev2.demoproject.config.TaskChannel;
import com.mantech.dev2.demoproject.model.TaskEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
public class EventMonitorService {
    private final TaskChannel taskChannel;
    Logger logger = LoggerFactory.getLogger(EventMonitorService.class);

    @Autowired
    public EventMonitorService(TaskChannel taskChannel) {
        this.taskChannel = taskChannel;
    }

    @StreamListener(
            target = TaskChannel.INPUT)
    public void onTaskEvent(TaskEvent taskEvent) {
        logger.info("onTaskEvent type:{}, name:{}, description:{}", taskEvent.getName(), taskEvent.getType(), taskEvent.getDescription());
    }
}
